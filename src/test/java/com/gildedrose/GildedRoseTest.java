package com.gildedrose;

import org.approvaltests.combinations.CombinationApprovals;
import org.junit.jupiter.api.Test;

class GildedRoseTest {

    @Test
    void foo() {
        // GIVEN
        String[] givenNames = {"foo", "Aged Brie", "Backstage passes to a TAFKAL80ETC concert", "Sulfuras, Hand of Ragnaros"};
        Integer[] givenSellInValues = {-1, 0, 3, 6, 11};
        Integer[] givenQualityValues = {0, 1, 49, 50};

        // WHEN & THEN
        CombinationApprovals.verifyAllCombinations(
            (name, sellIn, quality) -> {
                Item[] items = new Item[]{Item.createItem(name, sellIn, quality)};
                GildedRose app = new GildedRose(items);
                app.updateQuality();
                return app.items[0].toString();
            },
            givenNames,
            givenSellInValues,
            givenQualityValues
        );
    }

}
